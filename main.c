
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


FILE * openFile(char pAddress[])
{
    FILE *Data;
    Data = fopen(pAddress,"r");
    if(Data == NULL) {
        perror("fopen");
        return NULL;
    }
    return Data;
}


char * getTri(char pBuff[], int pIndex)
{
    static char tri[3];
    int i;
    for(i = 0; i < 3; i++)
    {
        if(pBuff[pIndex] != ';')
        {
            tri[i] = pBuff[pIndex];
            pIndex++;
        } else
        {
            tri[0] = ';';
            return tri;
        }
    }
    return tri;
}


char * findAminoacid(char pCodon[])
{
    FILE *Data = openFile("DNA_Data");
    if(Data == NULL)
        return NULL;

    static char buff[255];

    while(fscanf(Data, "%s", buff) != -1)
    {
        int read_index = 0;
        char *tri = getTri(buff, 0);
        while(tri[0] != ';')
        {
            if(strcmp(pCodon, tri) != 0) {
                read_index += 3;
                tri = getTri(buff, read_index);
            } else {
                fscanf(Data, "%s", buff);
                fclose(Data);
                return buff;
            }
        }
        fscanf(Data, "%s", buff);
    }

    fclose(Data);
    return NULL;
}


int isValid(char pChar)
{
    char valid_characters[] = {'A', 'C', 'T', 'G'};
    int i;
    for(i = 0; i < 4; i++)
    {
        if(pChar == valid_characters[i])
            return 1;
    }
    return 0;
}


char * translateToRNA(char pCodon[])
{
    static char translation[3];
    if(strlen(pCodon) == 3)
    {
        int i;
        for(i = 0; i < 3; i++)
        {
            if(isValid(pCodon[i]) == 1)
            {
                if(pCodon[i] != 'T')
                    translation[i] = pCodon[i];
                else
                    translation[i] = 'U';
            } else
            {
                printf("Invalid DNA Character");
                return NULL;
            }
        }
        return translation;
    }
    printf("Invalid length");
    return NULL;
}


void inputDNA(char pFileAddress[])
{
    FILE *Input = openFile(pFileAddress);
    if(Input == NULL)
        return;

    char buff[255];

    while(fscanf(Input, "%s", buff) != -1)
    {
        char *translated = translateToRNA(buff);
        if (translated != NULL)
            printf("%s\n", findAminoacid(translated));
        else
            return;
    }
}


typedef struct node {
    char val[3];
    struct node * next;
} node_t;


typedef struct list {
    node_t * sub_list;
    struct list * next;
}list_t;


void print_list(node_t * list) {
    node_t * current = list;
    while (current != NULL) {
        printf("%s ", current->val);
        current = current->next;
    }
    printf("-> %s", findAminoacid(list->val));
}


void print_listV2(list_t * list) {
    list_t * current = list;
    while (current != NULL) {
        print_list(current->sub_list);
        current = current->next;
        printf("\n");
    }
}


void findAminoRNA(char pFileAddress[])
{
    FILE *Input = openFile(pFileAddress);
    if(Input == NULL)
        return;
    char amino_acid[255];

    FILE *Data = openFile("DNA_Data");
    if(Data == NULL)
        return;

    char RNA[255];
    char Amino[255];

    list_t * master_list = malloc(sizeof(list_t));
    list_t * master_current = master_list;

    while(fscanf(Input, "%s", amino_acid) != -1) {
        while (fscanf(Data, "%s", RNA) != -1) {
            node_t *first_list = malloc(sizeof(node_t));
            node_t *current = first_list;
            fscanf(Data, "%s", Amino);
            if (strcmp(amino_acid, Amino) == 0) {
                int i = 0;
                int x;
                for (x = 0; x < strlen(RNA) - 1; x++) {
                    current->val[i] = RNA[x];
                    i = i + 1;
                    if (i == 3) {
                        current->next = malloc(sizeof(node_t));
                        current = current->next;
                        i = 0;
                    }
                }
                master_current->sub_list = first_list;
                master_current->next = malloc(sizeof(list_t));
                master_current = master_current->next;
                break;
            }
        }
    }
    print_listV2(master_list);
}



int main()
{
    inputDNA("Input_DNA.txt");
    printf("1) Traducir ADN -> Aminoacidos\n");
    printf("2) Traducir Aminoaciods -> ADN\n");
    char opcion[100];
    printf( "Escoja una opcion: ");
    scanf("%s", opcion);
    if(opcion[1] == "1")
    {
        printf("Digite el nombre del archivo: ");
        scanf("%s", opcion);
        inputDNA(opcion);
    } else
    {
        printf("Digite el nombre del archivo: ");
        scanf("%s", opcion);
        findAminoRNA(opcion);
    }
    return 0;
}
